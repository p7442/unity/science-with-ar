﻿//----------------------------------------------------------------------------
//  Copyright (C) 2022 by FutureLabY. All rights reserved. 
//  Programmer : Taher Hassan
//  Licensed under the MIT license.
//----------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiEvents : MonoBehaviour {
	public bool take_control = false ; 
	public Animator Buttons_anim ; 
	public int currentPlanet;
	public void Earth_control ()
	{
		take_control = true; 
	}


	public void Earth_Rotate ()
	{
		take_control = false; 
	}
}
