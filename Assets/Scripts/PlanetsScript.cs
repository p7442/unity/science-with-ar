﻿//----------------------------------------------------------------------------
//  Copyright (C) 2022 by FutureLabY. All rights reserved. 
//  Programmer : Taher Hassan
//  Licensed under the MIT license.
//----------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class PlanetsScript : MonoBehaviour {

    public float speed_of_rotation;
    public Animator pop_anim;
    public Animator Planet_UI_anim;
    public GameObject Planet;
    public GameObject Planet_ui;
    public int planetNumber;
    int lastPlanetNumber;

    //Scale
    Vector3 pos_1, pos_2;
    float dist, old_dist, x_scale, y_scale, z_scale, delta;
    float Delta_factor = 200; // the higher the number the slower the scale 

    //rotate 
    Vector3 last_pos;
    Vector3 new_pos;
    Touch touch;
    float Rotate_factor = 0.1f; // the lower the number the slower the rotation 
    GameObject player_1;
    GuiEvents GUI_events_script;

    void Start()
    {
        x_scale = Planet.transform.localScale.x;
        y_scale = Planet.transform.localScale.y;
        z_scale = Planet.transform.localScale.z;

        player_1 = GameObject.FindGameObjectWithTag("GUI_events_tag");
        GUI_events_script = player_1.GetComponent<GuiEvents>();
    }

    public void TargetFound()
    {
        Planet_ui.SetActive(true);
        Planet_UI_anim.SetBool("Planet_is_on", true);
        GUI_events_script.currentPlanet = planetNumber;

        if (lastPlanetNumber != planetNumber)
        {
            Planet_UI_anim.SetBool("Planet_is_on", true);
            Planet_ui.SetActive(true);
            pop_anim.SetBool("Planet_anim_bool", true);
            StartCoroutine(earth_pop_w8(1f));
        }

        lastPlanetNumber = planetNumber;
    }

    public void TargetLost()
    {
        Planet_UI_anim.SetBool("Planet_is_on", false);
    }

    IEnumerator earth_pop_w8(float time)
    {
        yield return new WaitForSeconds(time);
        pop_anim.SetBool("Planet_anim_bool", false);
    }



    void Update()
    {


        if (!GUI_events_script.take_control)
            Planet.transform.Rotate(speed_of_rotation * Vector3.up * Time.deltaTime);
        else
        {
            //////Rotate manuly
            if (Input.touches.Length == 1)
            {
                touch = Input.touches[0];

                if (touch.phase == TouchPhase.Began)
                {
                    new_pos = touch.position;
                    last_pos = new_pos;
                }

                if (touch.phase == TouchPhase.Moved)
                {
                    new_pos = touch.position;
                    //	Planet.transform.Rotate ( (new_pos.y - last_pos.y)*Rotate_factor , -(new_pos.x - last_pos.x)*Rotate_factor , -(new_pos.z - last_pos.z)*Rotate_factor , Space.World) ;
                    Planet.transform.Rotate((new_pos.y - last_pos.y) * Rotate_factor, -(new_pos.z - last_pos.z) * Rotate_factor, -(new_pos.x - last_pos.x) * Rotate_factor, Space.World);
                    last_pos = new_pos;
                }

            }else 

            ///// Scale

            if (Input.touches.Length == 2 && Input.touches[1].phase == TouchPhase.Began)
            {
                pos_1 = Input.touches[0].position;
                pos_2 = Input.touches[1].position;

                old_dist = Mathf.Sqrt(Mathf.Pow(pos_1.x - pos_2.x, 2) + Mathf.Pow(pos_1.y - pos_2.y, 2) + Mathf.Pow(pos_1.z - pos_2.z, 2));
            }


            if (Input.touches.Length == 2 && (Input.touches[0].phase == TouchPhase.Moved || Input.touches[1].phase == TouchPhase.Moved))
            {

                pos_1 = Input.touches[0].position;
                pos_2 = Input.touches[1].position;

                dist = Mathf.Sqrt(Mathf.Pow(pos_1.x - pos_2.x, 2) + Mathf.Pow(pos_1.y - pos_2.y, 2) + Mathf.Pow(pos_1.z - pos_2.z, 2));
                delta = (dist - old_dist) / Delta_factor;

                if ((x_scale + delta) > 0 && (y_scale + delta) < 1.2f)
                    Planet.transform.localScale = new Vector3(x_scale + delta, y_scale + delta, z_scale + delta);

                x_scale = Planet.transform.localScale.x;
                y_scale = Planet.transform.localScale.y;
                z_scale = Planet.transform.localScale.z;

                old_dist = dist;
            }
        }

    }
}
