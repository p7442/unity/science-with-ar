﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadScene : MonoBehaviour {
    
    public void AnimalsButton ()
    {
        SceneManager.LoadScene(2);
    }

    public void SpaceButton ()
    {
        SceneManager.LoadScene(1);
    }

    public void BackButton ()
    {
        SceneManager.LoadScene(0);
    }
}
