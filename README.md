# Science with AR
"Science with AR" is an Augmented Reality project dedicated to helping kids to learn more about planets and animals, where users can choose one of the two current subjects to study (Astronomy and Zoology).
Choosing a subject will open the mobile camera expecting the user to put the image of (a planet or an animal) to show related information.

For now, there are 8 planets (Sun, Mercury, Venus, Earth, Mars, Jupiter, Saturn, and Uranus) and 3 animals (Tiger, Duck, and Wolf).

The project can support Engilsh and Arabic.
## How to use?
You can find the markers for AR in the folder "Markers" in the root directory of the project, you have to print those markers and put them in front of the AR camera in order to see the information.<br />
**Note**: you can find the .APK files in the "APK" folder located in the root directory, there are 2 APKs, "Science with AR.apk" is in Arabic and "Science with AR_Eng.apk" is for the English version.

## For developers:
The project was originally made with Unity 2018.1 but was upgraded to Unity 2020.3 before the initial push.<br />

Vuforia 10.3.2 is used for the marker-based AR. **Note**: you need to use your own "App License Key" in order to make your build's tracking work.<br />

After adding the Vuforia license you should be able to build with no problem(was tested for android only).
As the project is old, there are many improvements that can be made, feel free to contact me and contribute to this repo.

This project can be used either in Arabic or in English. Arabic learning information are images can be found in "Assets\Images", you can add your images to change data.

Now This project support english and the information is a text added into the UI foreach planet.

Feel free to contribute to this project by updating the UI and information to make it more dynamic.
## About the creator
This project was created by "Taher Hassan" in 2018 and uploaded to GitLab in 2022 to archive and try to help whoever may be working on a similar project and may get use of it, the upgrade from 2018 to 2022 has not considered improving the project, so there are a lot of improvements that could be done.

Feel free to contact me on:<br />
**LinkedIn**: [Taher Hassan](https://www.linkedin.com/in/taher-hassan/).<br />
**Gmail**: taher.w.hassan@gmail.com<br />

[Future laby](https://futurelaby.com/) is a startup working on educational AR/VR applications, this project was one of its first projects, if you are interested please visit our website or message us on [Facebook](https://www.facebook.com/FutureLabY) or [LinkedIn]()

## License
[MIT](https://choosealicense.com/licenses/mit/)
